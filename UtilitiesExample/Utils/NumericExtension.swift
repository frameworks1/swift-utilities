//
//  NumericExtension.swift
//  UtilitiesExample
//
//  Created by Imthath M on 15/07/19.
//  Copyright © 2019 Zoho Corp. All rights reserved.
//

import Foundation

extension Numeric {
    
    private func toNSNumber() -> NSNumber? {
        if let num = self as? NSNumber { return num }
        guard let string = self as? String, let double = Double(string) else { return nil }
        return NSNumber(value: double)
    }
    
    func precision(_ minimumFractionDigits: Int, minimumIntegerDigits: Int = 1,
                   roundingMode: NumberFormatter.RoundingMode = NumberFormatter.RoundingMode.halfUp) -> String {
        guard let number = toNSNumber() else { return "\(self)" }
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = minimumFractionDigits
        formatter.roundingMode = roundingMode
        formatter.minimumIntegerDigits = minimumIntegerDigits
        return formatter.string(from: number) ?? "\(self)"
    }
    
    func precision(with numberFormatter: NumberFormatter) -> String? {
        guard let number = toNSNumber() else { return nil }
        return numberFormatter.string(from: number)
    }
}
